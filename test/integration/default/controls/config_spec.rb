# frozen_string_literal: true

control 'polybar-config-file-user-auser-present' do
  title 'should be present'

  describe user('auser') do
    it { should exist }
  end
end

control 'polybar-config-file-config-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/polybar') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'polybar-config-file-polybar-config-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/polybar/config.ini') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('; Your changes will be overwritten.') }
    its('content') { should include('; Include powerline utility') }
  end
end

control 'polybar-config-file-polybar-deprecated-config-auser-removed' do
  title 'should should not exist'

  describe file('/home/auser/.config/polybar/config') do
    it { should_not exist }
  end
end

control 'polybar-config-file-polybar-config-powerline-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/polybar/powerline-for-polybar.config') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('; Your changes will be overwritten.') }
    its('content') { should include('; # Left-to-right starting arrow') }
  end
end

control 'polybar-config-file-polybar-config-colors-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/polybar/colors.config') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('; Your changes will be overwritten.') }
    its('content') { should include('[colors]') }
  end
end

control 'polybar-config-file-i3-config-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/i3') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'polybar-config-file-i3-config-autostart-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/i3/autostart.d') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'polybar-config-file-i3-config-autostart-script-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/i3/autostart.d/polybar') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('killall -q polybar') }
  end
end
