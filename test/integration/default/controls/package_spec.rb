# frozen_string_literal: true

control 'polybar-package-install-pkg-installed' do
  title 'should be installed'

  describe package('polybar') do
    it { should be_installed }
  end
end
