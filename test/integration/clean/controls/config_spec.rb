# frozen_string_literal: true

control 'polybar-config-clean-i3-config-autostart-script-auser-absent' do
  title 'should not exist'

  describe file('/home/auser/.config/i3/autostart.d/polybar') do
    it { should_not exist }
  end
end

control 'polybar-config-clean-polybar-config-colors-auser-absent' do
  title 'should not exist'

  describe file('/home/auser/.config/polybar/colors.config') do
    it { should_not exist }
  end
end

control 'polybar-config-clean-polybar-config-powerline-auser-absent' do
  title 'should not exist'

  describe file('/home/auser/.config/polybar/powerline-for-polybar.config') do
    it { should_not exist }
  end
end

control 'polybar-config-clean-polybar-config-auser-absent' do
  title 'should not exist'

  describe file('/home/auser/.config/polybar/config') do
    it { should_not exist }
  end
end

control 'polybar-config-clean-polybar-dir-auser-absent' do
  title 'should not exist'

  describe directory('/home/auser/.config/polybar') do
    it { should_not exist }
  end
end
