# frozen_string_literal: true

control 'polybar-package-clean-pkg-absent' do
  title 'should not be installed'

  describe package('polybar') do
    it { should_not be_installed }
  end
end
