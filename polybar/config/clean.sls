# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as polybar with context %}

{% if salt['pillar.get']('polybar-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_polybar', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

polybar-config-clean-i3-config-autostart-script-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/i3/autostart.d/polybar

polybar-config-clean-polybar-config-colors-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/polybar/colors.config

polybar-config-clean-polybar-config-powerline-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/polybar/powerline-for-polybar.config

polybar-config-clean-polybar-config-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/polybar/config

polybar-config-clean-polybar-dir-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/polybar

{% endif %}
{% endfor %}
{% endif %}
