# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import mapdata as polybar with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_package_install }}

{% if salt['pillar.get']('polybar-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_polybar', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

polybar-config-file-user-{{ name }}-present:
  user.present:
    - name: {{ name }}

polybar-config-file-polybar-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/polybar
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - polybar-config-file-user-{{ name }}-present

polybar-config-file-polybar-config-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/polybar/config.ini
    - source: {{ files_switch([
                  name ~ '-polybar-config.ini.tmpl',
                  'polybar-config.ini.tmpl'],
                lookup='polybar-config-file-polybar-config-' ~ name ~ '-managed',
                )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - polybar-config-file-polybar-dir-{{ name }}-managed

polybar-config-file-polybar-deprecated-config-{{ name }}-removed:
  file.absent:
    - name: {{ home }}/.config/polybar/config

polybar-config-file-polybar-config-powerline-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/polybar/powerline-for-polybar.config
    - source: {{ files_switch([
                  name ~ 'powerline-for-polybar.config.tmpl',
                  'powerline-for-polybar.config.tmpl'],
                lookup='polybar-config-file-polybar-config-powerline-' ~ name ~ '-managed',
                )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - polybar-config-file-polybar-dir-{{ name }}-managed

polybar-config-file-polybar-config-colors-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/polybar/colors.config
    - source: {{ files_switch([
                  name ~ 'colors.config.tmpl',
                  'colors.config.tmpl'],
                lookup='polybar-config-file-polybar-config-colors-' ~ name ~ '-managed',
                )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - polybar-config-file-polybar-dir-{{ name }}-managed

polybar-config-file-i3-config-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/i3
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - polybar-config-file-user-{{ name }}-present

polybar-config-file-i3-config-autostart-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/i3/autostart.d
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - polybar-config-file-i3-config-dir-{{ name }}-managed

polybar-config-file-i3-config-autostart-script-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/i3/autostart.d/polybar
    - source: {{ files_switch([
                   name ~ '-polybar-autostart.sh.tmpl', 'polybar-autostart.sh.tmpl'],
                 lookup='polybar-config-file-i3-config-autostart-script-' ~ name ~ '-managed'
                 )
              }}
    - mode: '0755'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - polybar-config-file-i3-config-autostart-dir-{{ name }}-managed
{% endif %}
{% endfor %}
{% endif %}
