# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as polybar with context %}

polybar-package-install-pkg-installed:
  pkg.installed:
    - name: {{ polybar.pkg.name }}
